root := $(patsubst %/,%,$(dir $(lastword ${MAKEFILE_LIST})))

%/:
	$(if $(wildcard $@.),@echo $@ exists,mkdir $@)


# SSL key, csr and cert generation for local development use
override SSL_C  ?= MX
override SSL_ST ?= CDMX
override SSL_L  ?= CDMX
override SSL_O  ?= me
override SSL_OU ?= me
override SSL_CN ?= localhost
SSL_SUBJ :=
SSL_SUBJ := ${SSL_SUBJ}//C=${SSL_C}
SSL_SUBJ := ${SSL_SUBJ}\ST=${SSL_ST}
SSL_SUBJ := ${SSL_SUBJ}\L=${SSL_L}
SSL_SUBJ := ${SSL_SUBJ}\O=${SSL_O}
SSL_SUBJ := ${SSL_SUBJ}\OU=${SSL_OU}
SSL_SUBJ := ${SSL_SUBJ}\CN=${SSL_CN}

.PHONY: ssl-gen
ssl-gen: ${root}/ssl/key.pem
ssl-gen: ${root}/ssl/csr.pem
ssl-gen: ${root}/ssl/cert.pem
ssl-gen:
	openssl rsa  -in $(word 1, $^) -check
	openssl req  -in $(word 2, $^) -text -noout -verify
	openssl x509 -in $(word 3, $^) -text -noout

${root}/ssl/key.pem: | ${root}/ssl/
	openssl genrsa -out $@ 1024

${root}/ssl/csr.pem: ${root}/ssl/key.pem
	openssl req -new -key $< -out $@ \
		-subj "${SSL_SUBJ}"

${root}/ssl/cert.pem: ${root}/ssl/key.pem ${root}/ssl/csr.pem
	openssl x509 -req \
		-signkey $(word 1, $^) \
		-in      $(word 2, $^) \
		-out $@
